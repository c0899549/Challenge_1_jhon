from flask import Flask, jsonify
from dotenv import load_dotenv
import os

load_dotenv()

app = Flask(__name__)

@app.route('/')
def authenticate():
    full_name = "YOUR_FULL_NAME"
    return f"Welcome {full_name}! You are authenticated to use the API."

@app.route('/store_data')
def store_data():
    USERNAME = os.getenv("USERNAME")
    PASSWORD = os.getenv("PASSWORD")
    CLUSTER_URL = os.getenv("CLUSTER_URL")
    DATA = {"age": 50, "height": 182, "weight": 85}

    response = {
        "CLUSTER_URL": f"mongodb://{USERNAME}:{PASSWORD}@{CLUSTER_URL}",
        "DATA": DATA
    }
    return jsonify(response)

if __name__ == '__main__':
    app.run(debug=True)
